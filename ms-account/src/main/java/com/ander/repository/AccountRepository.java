package com.ander.repository;
import com.ander.entity.Account;
import org.springframework.data.repository.CrudRepository;
public interface AccountRepository extends CrudRepository<Account,Integer> {

}

