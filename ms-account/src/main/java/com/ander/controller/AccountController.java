package com.ander.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ander.entity.Account;
import com.ander.service.IAccountService;
@RestController
@RequestMapping("/account")
public class AccountController {	
    @Autowired
    private IAccountService service;

    @GetMapping("/listar")
    public List<Account> listar() {
        return service.findAll();
    }

    @GetMapping("/ver/{id}")
    public Account listarPorID(@PathVariable Integer id) {
        Account account = service.findById(id);
        return account;
    }
    
    @PostMapping("/registrar")
    public void save (@RequestBody Account account) {
    	 service.save(account);    	   	
    }
    
    @PostMapping("/update")
    public void update (@RequestBody Account account) {
    	 service.update(account);    	   	
    }
    
    @DeleteMapping("/eliminar/{id}")
    public void delete (@PathVariable Integer id) {
    	service.delete(id);
    }   
    
    
//    @PostMapping("registrar")
//    public Account save (@RequestBody Account account) {
//    	Account acc = service.save(account);
//    	return acc;    	
//    }
    



}