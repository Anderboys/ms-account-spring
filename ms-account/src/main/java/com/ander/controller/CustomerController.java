package com.ander.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ander.entity.Customer;
import com.ander.service.ICustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {	
	
    @Autowired
    private ICustomerService serviceCustomer;

    @GetMapping("/listar")
    public List<Customer> listar() {
        return serviceCustomer.findAll();
    }
    
    @GetMapping("/ver/{id}")
    public Customer listarPorID(@PathVariable Integer id) {
    	Customer customer = serviceCustomer.findById(id);
        return customer;
    }
    
    @PostMapping("/registrar")
    public String save (@RequestBody Customer customer) {
    	serviceCustomer.save(customer); 
    	return "Cliente registrado Correctamente" 
    	+ "\nID:"+customer.getIdCustomer()
    	+ "\nNombre:"+customer.getNombres();
    	
    }         
    @PostMapping("/update")
    public String update (@RequestBody Customer customer) {
    	serviceCustomer.update(customer);
    	return "Cliente Actualizado Correctamente" + "\nID:"+customer.getIdCustomer();
    }   
        
    @DeleteMapping("/eliminar/{id}")
    public String deleteByID(@PathVariable Integer id) {
    	serviceCustomer.delete(id);
		 return "delete Succsefully id = " +id;
	}
    
//    @PostMapping("/registrar")
//    public void save (@RequestBody Customer customer) {
//    	serviceCustomer.save(customer);    	   	
//    }   
    
//    @PostMapping("/update")
//    public void update (@RequestBody Customer customer) {
//    	serviceCustomer.update(customer);     	
//    }
    
//   @DeleteMapping("/eliminar/{id}")
//   public void delete (@PathVariable Integer id) {
//  	serviceCustomer.delete(id);
//   }
    
    @PostMapping("/saveOrupdate")
  	public Customer SaveOrUpdate(@RequestBody Customer customer) {		
      	serviceCustomer.saveorUpdate(customer);
  		return customer;
  	}
    
  
    
  

}
