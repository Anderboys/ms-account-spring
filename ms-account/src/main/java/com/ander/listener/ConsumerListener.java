package com.ander.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ander.service.TransactionEvents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;


@Component
public class ConsumerListener {

	//2. inyectamos TransactionEvents para que lea el topico:  transaction-events -> de: ms-deposit
    @Autowired
    private TransactionEvents _transactionEvents;

    private Logger log = LoggerFactory.getLogger(ConsumerListener.class);

 
    // buscar info en google sobre KafkaListener

    // ESCUCHE TOPICO DEPOSITO 
    @KafkaListener(topics = {"transaction-events"})
    
    
    //                               consumerRecord = Registro de Consumo
    public void OnMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonMappingException, JsonProcessingException {
        log.info("*************** MICROSERVICE ACCOUNT *******************");
      
        //com.ander.listener.ConsumerListener
        // : ConsumerRecord : {"id":2,"amount":234.0,"type":"deposito","accountId":1}
        log.info("ConsumerRecord : {}", consumerRecord.value());
        _transactionEvents.processTransactionEvent(consumerRecord);
    }
    
    
}

