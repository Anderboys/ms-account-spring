package com.ander.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ander.entity.Customer;
import com.ander.repository.CustomerRepository;
@Service
public class CustomerServiceImpl implements ICustomerService {
	
	@Autowired
	CustomerRepository _customerRepo;

	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		 return  (List<Customer>)_customerRepo.findAll();
	}

	@Override
	public Customer findById(Integer id) {
		// TODO Auto-generated method stub
		return _customerRepo.findById(id).orElse(null);
	}
	
	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		_customerRepo.deleteById(id);		
	}

	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub
		_customerRepo.save(customer);		
	}

	@Override
	public void update(Customer customer) {
		// TODO Auto-generated method stub
		save(customer);
	}
		
	@Override
	public void saveorUpdate(Customer customer) {
		_customerRepo.save(customer);
		
	}	

	
}
