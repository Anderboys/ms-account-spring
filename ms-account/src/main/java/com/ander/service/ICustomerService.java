package com.ander.service;

import java.util.List;
import com.ander.entity.Customer;

public interface ICustomerService {
	
    public List<Customer> findAll();
    public Customer findById (Integer id );
    void delete (Integer id );
    
    void save (Customer customer);
    void update (Customer customer);   
      
 	
   	// Registrar or Update
   	public void saveorUpdate(Customer customer);   	
  

}
