package com.ander.service;
import com.ander.entity.Account;
import com.ander.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AccountServiceImpl implements IAccountService{

    @Autowired
    AccountRepository _accountRepo;

    @Override
    public List<Account> findAll() {
        // TODO Auto-generated method stub
        return  (List<Account>)_accountRepo.findAll();
    }

    @Override
    public Account findById(Integer id) {
        // TODO Auto-generated method stub
        return _accountRepo.findById(id).orElse(null);
    }

//    @Override
//    public Account save(Account account) {
//        // TODO Auto-generated method stub
//        return _accountRepo.save(account);
//    }
    
    @Override
    public void save(Account account) {
        // TODO Auto-generated method stub
         _accountRepo.save(account);
    }

	@Override
	public void update(Account account) {
		// TODO Auto-generated method stub
		save(account);		
	}

	@Override
	public void delete(Integer id) {
		_accountRepo.deleteById(id);
		
	}

}

