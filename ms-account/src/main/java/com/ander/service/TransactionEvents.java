package com.ander.service;

import org.springframework.stereotype.Service;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.ander.entity.Account;
import com.ander.entity.TransactionDeposit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


//DETALLE: PRIMERO CREAMOS EL PRODUCER -> (ms-deposit) LUEGO EL CONSUMER -> (ms-account)
//1. TransactionEvents trabajar con kafka -> para que escuche

//TransactionEventCONSUMER
@Service
public class TransactionEvents {

    @Autowired
    private IAccountService accountService ;

    private Logger log = LoggerFactory.getLogger(TransactionEvents.class);

    @Autowired
    private ObjectMapper objectMapper ;

    //                                              consumerRecord = Registro de Consumo
    public void processTransactionEvent(ConsumerRecord<Integer, String> registarConsumo) throws JsonMappingException, JsonProcessingException {

        double newAmount= 0 ;
        
        // ms-account
        Account account = new Account();
        
        // ms-deposit
       // Transaction event = objectMapper.readValue(consumerRecord.value(), TransactionDeposit.class);
        
        TransactionDeposit transactionDeposit = objectMapper.readValue(registarConsumo.value(), TransactionDeposit.class);
        
        
        account = accountService.findById(transactionDeposit.getAccountId());

        switch(transactionDeposit.getType()) {
            case "deposito":
                newAmount =  account.getTotalAmount() + transactionDeposit.getAmount();
                break ;

            case "retiro":
                newAmount =  account.getTotalAmount() - transactionDeposit.getAmount();
                break ;

        }
        
        account.setTotalAmount(newAmount);
        log.info("Actulizando N° cuenta *** "+transactionDeposit.getAccountId());
        accountService.save(account);
    }

}

