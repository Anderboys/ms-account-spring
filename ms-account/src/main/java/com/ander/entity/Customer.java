package com.ander.entity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customer")
    private Integer IdCustomer;
    
    @Column(name = "nombres")
    private String Nombres;
    
    @Column(name = "apellidos")
    private String Apellidos;

    @Column(name = "direccion")
    private String Direccion;
    
    @Column(name = "telefono")
    private Integer Telefono;
    
    @Column(name = "estado")
    private String Estado;
  
    @Column(name = "dni")
    private Integer Dni;
    
    @Column(name = "email")
    private String Email;
    
    @Column(name = "fecha_registro")
    private String FechaRegistro; 

    
    
    public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getFechaRegistro() {
		return FechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		FechaRegistro = fechaRegistro;
	}

	public Integer getDni() {
		return Dni;
	}

	public void setDni(Integer dni) {
		Dni = dni;
	}


	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public Integer getTelefono() {
		return Telefono;
	}

	public void setTelefono(Integer telefono) {
		Telefono = telefono;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	  
    public Integer getIdCustomer() {
        return IdCustomer;
    }
	
	public void setIdCustomer(Integer idCustomer) {
        IdCustomer = idCustomer;
    }

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

   


   
}
